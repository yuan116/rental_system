package app.core;

import java.util.HashMap;

public class Util {
    public static HashMap getPropertyTypes() {
        HashMap<PropertyTypes, String> propertyTypeMap = new HashMap<PropertyTypes, String>();

        propertyTypeMap.put(PropertyTypes.CONDOMINIUM, "Condominium");
        propertyTypeMap.put(PropertyTypes.SINGLE_STOREY, "Single Storey");
        propertyTypeMap.put(PropertyTypes.DOUBLE_STOREY, "Double Storey");
        propertyTypeMap.put(PropertyTypes.TOWN_HOUSE, "Town House");
        propertyTypeMap.put(PropertyTypes.BUNGALOW, "Bungalow");

        return propertyTypeMap;
    }

    public static HashMap getUserTypes() {
        HashMap<UserTypes, String> userTypeMap = new HashMap<UserTypes, String>();

        userTypeMap.put(UserTypes.ADMINISTRATOR, "Administrator");
        userTypeMap.put(UserTypes.OWNER, "Owner");
        userTypeMap.put(UserTypes.AGENT, "Agent");
        userTypeMap.put(UserTypes.TENANT, "Tenant");

        return userTypeMap;
    }

    public enum PropertyTypes {
        CONDOMINIUM,
        SINGLE_STOREY,
        DOUBLE_STOREY,
        TOWN_HOUSE,
        BUNGALOW
    }


    public enum UserTypes {
        ADMINISTRATOR,
        OWNER,
        AGENT,
        TENANT
    }

    public enum SignUpStatus {
        PENDING,
        REJECTED,
        RESUBMIT
    }
}
