package app.core;

import java.awt.event.ActionListener;
import java.util.HashMap;

public abstract class Controller implements ActionListener {
    protected HashMap<String, Object> data;

    protected String upperCaseFirstCharacter(String value) {
        return value.substring(0, 1).toUpperCase() + value.substring(1);
    }

}
