package app.core;

import java.util.HashMap;

public class Session {
    protected static HashMap<String, String> data = new HashMap<String, String>();

    //https://stackoverflow.com/questions/2914695/how-can-you-write-a-function-that-accepts-multiple-types/2914730
    public static <Template> void setData(String key, Template value) {
        data.put(key, value.toString());
    }

    public static String getData(String key) {
        return data.get(key);
    }

    public static void deleteData(String key) {
        data.remove(key);
    }

    public static void clearData() {
        data.clear();
    }
}
