package app.core;

import app.RentalSystem;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

public class View extends JFrame {
    protected HashMap<String, Object> actionListenerData = new HashMap<String, Object>();

    public View() {
        setTitle("Rental System");
        setLocation(500, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(false);

        //https://stackoverflow.com/questions/16372241/run-function-on-jframe-close/16372860
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                RentalSystem.exitApplication();
            }
        });
    }
}
