package app.core;

public abstract class Model {
    protected FileSystem fileSystem;

    protected void initFileSystem(String filename) {
        fileSystem = new FileSystem(filename);
    }

    abstract public void readDataFromFile();

    abstract public void writeDataToFile();

    public void exitApplication() {
        writeDataToFile();
    }
}
