package app.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class FileSystem {
    protected final String PATH = "records" + System.getProperty("file.separator");
    protected final String EXTENSION = ".txt";
    protected final String END_HEADER_STRING = "_END_OF_HEADER_";
    protected File file;

    public FileSystem(String filename) {
        if (filename.equals("")) {
            throw new IllegalArgumentException("Filename cannot be empty string.");
        }

        file = new File(PATH + filename + EXTENSION);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException exception) {
                System.out.printf(
                        "Please manually create '%s%s' in '%s' directory.",
                        filename,
                        EXTENSION,
                        PATH
                );

                System.exit(1);
            }
        }
    }

    public ArrayList<HashMap<String, String>> readFile() {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> row = new HashMap<String, String>();

        try {
            Scanner fileInput = new Scanner(file);

            ArrayList<String> headers = new ArrayList<String>();
            boolean startHeader = true;
            String fileInputString = "";

            while (fileInput.hasNext()) {
                fileInputString = fileInput.nextLine().trim();

                if (startHeader) {
                    if (fileInputString.equals(END_HEADER_STRING)) {
                        startHeader = false;
                        continue;
                    }

                    headers.add(fileInputString);
                    continue;
                }

                headers.forEach((header) -> row.put(header, fileInput.nextLine().trim()));
                list.add(row);
            }

            fileInput.close();
        } catch (FileNotFoundException exception) {
            System.out.println(exception.getMessage());
            System.exit(1);
        }

        return list;
    }

    //https://stackoverflow.com/questions/2914695/how-can-you-write-a-function-that-accepts-multiple-types/2914730
    public <Template> void writeFile(String[] headers, ArrayList<HashMap<String, Template>> list) {
        if (headers.length == 0) {
            throw new IllegalArgumentException("File data headers cannot be empty.");
        }

        try {
            PrintWriter fileOutput = new PrintWriter(file);

            for (String header : headers) {
                fileOutput.println(header);
            }
            fileOutput.println(END_HEADER_STRING);
            fileOutput.println();

            list.forEach((row) -> {
                for (String header : headers) {
                    Template value = row.get(header);
                    fileOutput.println(value == null ? "" : value);
                }
                fileOutput.println();
            });

            fileOutput.flush();
            fileOutput.close();
        } catch (FileNotFoundException exception) {
            System.out.println(exception.getMessage());
            System.exit(1);
        }
    }
}
