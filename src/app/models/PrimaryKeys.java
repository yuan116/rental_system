package app.models;

import app.core.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class PrimaryKeys extends Model {
    private final String FILENAME = "primary_keys";

    protected HashMap<String, Integer> data = new HashMap<String, Integer>();

    protected String[] headers = {
            "user_id"
    };

    public PrimaryKeys() {
        initFileSystem(FILENAME);
        readDataFromFile();
    }

    public int get(String key) {
        checkKeyExist(key);
        return data.get(key);
    }

    public String getIncrement(String key) {
        checkKeyExist(key);
        int value = data.get(key);

        value++;
        data.put(key, value);
        writeDataToFile();

        return Integer.toString(value);
    }

    protected void checkKeyExist(String key) {
        if (!data.containsKey(key)) {
            throw new NoSuchElementException(String.format("Primary Key '%s' does not exist. Please add key to headers or check spelling.", key));
        }
    }

    @Override
    public void readDataFromFile() {
        ArrayList<HashMap<String, String>> list = fileSystem.readFile();

        if (list.isEmpty()) {
            for (String header : headers) {
                data.put(header, 0);
            }
        } else {
            list.get(0).forEach((primaryKey, value) -> data.put(primaryKey, value.equals("null") ? 0 : Integer.parseInt(value)));
        }
    }

    @Override
    public void writeDataToFile() {
        ArrayList<HashMap<String, Integer>> list = new ArrayList<HashMap<String, Integer>>();
        list.add(data);
        fileSystem.writeFile(headers, list);
    }
}
