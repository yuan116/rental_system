package app.models;

import app.RentalSystem;
import app.core.Model;

import java.util.ArrayList;
import java.util.HashMap;

public class Users extends Model {
    private final String FILENAME = "users";

    protected ArrayList<HashMap<String, String>> list;

    protected String[] headers = {
            "id",
            "username",
            "password",
            "user_types",
            "name",
            "phone_number",
            "identity_number"
    };

    protected String insertId = "0";

    public Users() {
        initFileSystem(FILENAME);
        readDataFromFile();
    }

    public void insert(HashMap<String, String> data) {
        PrimaryKeys primaryKeys = (PrimaryKeys) RentalSystem.getModel("primary_keys");

        data.put("id", insertId = primaryKeys.getIncrement("user_id"));
        list.add(data);
        writeDataToFile();
    }

    public String getInsertId(){
        return insertId;
    }

    public boolean usernameExist(String username) {
        for (HashMap<String, String> row : list) {
            if (row.get("username").equals(username)) {
                return true;
            }
        }

        return false;
    }

    public HashMap<String, String> get(int index) {
        return list.get(index);
    }

    public int getIndexByData(String key, String value) {
        int index = 0;
        for (HashMap<String, String> row : list) {
            if (row.get(key).equals(value)) {
                return index;
            }
            index++;
        }

        return -1;
    }

    @Override
    public void readDataFromFile() {
        list = fileSystem.readFile();
    }

    @Override
    public void writeDataToFile() {
        fileSystem.writeFile(headers, list);
    }
}
