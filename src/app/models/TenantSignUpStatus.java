package app.models;

import app.core.Model;

import java.util.ArrayList;
import java.util.HashMap;

public class TenantSignUpStatus extends Model {
    private final String FILENAME = "tenant_sign_up_status";

    protected ArrayList<HashMap<String, String>> list;

    protected String[] headers = {
            "user_id",
            "status",
            "reason",
            "message"
    };

    public TenantSignUpStatus() {
        initFileSystem(FILENAME);
        readDataFromFile();
    }

    public void insert(HashMap<String, String> data) {
        list.add(data);
        writeDataToFile();
    }

    @Override
    public void readDataFromFile() {
        list = fileSystem.readFile();
    }

    @Override
    public void writeDataToFile() {
        fileSystem.writeFile(headers, list);
    }
}
