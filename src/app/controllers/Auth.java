package app.controllers;

import app.RentalSystem;
import app.core.Controller;
import app.core.Session;
import app.core.Util;
import app.models.PrimaryKeys;
import app.models.TenantSignUpStatus;
import app.models.Users;
import app.views.auth.CheckSignUpStatus;
import app.views.auth.SignIn;
import app.views.auth.TenantSignUp;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;

public class Auth extends Controller {
    protected static SignIn viewSignIn = new SignIn();
    protected static TenantSignUp viewTenantSignUp = new TenantSignUp();
    protected static CheckSignUpStatus viewCheckSignUpStatus = new CheckSignUpStatus();

    public Auth() {

    }

    public Auth(HashMap data) {
        this.data = data;
    }

    public static void showViewSignIn() {
        viewTenantSignUp.setVisible(false);
        viewCheckSignUpStatus.setVisible(false);
        viewSignIn.setVisible(true);
    }

    public static void showViewTenantSignUp() {
        viewCheckSignUpStatus.setVisible(false);
        viewSignIn.setVisible(false);
        viewTenantSignUp.setVisible(true);
    }

    public static void showViewCheckSignUpStatus() {
        viewTenantSignUp.setVisible(false);
        viewSignIn.setVisible(false);
        viewCheckSignUpStatus.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        Object eventSource = event.getSource();

        if (eventSource == data.get("buttonGoToSignIn")) {
            showViewSignIn();
            return;
        } else if (eventSource == data.get("buttonGoToTenantSignUp")) {
            showViewTenantSignUp();
            return;
        } else if (eventSource == data.get("buttonGoToCheckSignUpStatus")) {
            showViewCheckSignUpStatus();
            return;
        }

        if (eventSource == data.get("buttonSignIn")) {
            signIn();
            return;
        } else if (eventSource == data.get("buttonTenantSignUp")) {
            tenantSignUp();
            return;
        } else if (eventSource == data.get("buttonCheckSignUpStatus")) {
            checkSignUpStatus();
            return;
        }
    }

    protected void signIn() {
        Users users = (Users) RentalSystem.getModel("users");
        String username, password;
        JTextField textFieldUsername = (JTextField) data.get("textFieldUsername");
        JTextField textFieldPassword = (JTextField) data.get("textFieldPassword");

        username = textFieldUsername.getText().trim();
        password = textFieldPassword.getText().trim();

        int index = users.getIndexByData("username", username);
        if (index == -1) {
            JOptionPane.showMessageDialog(
                    viewSignIn,
                    "Username not exist.",
                    "Failed to Sign In",
                    JOptionPane.WARNING_MESSAGE
            );
        } else {
            HashMap<String, String> data = users.get(index);

            if (data.get("password").equals(password)) {
                Session.setData("user_id", data.get("id"));
                Session.setData("user_types", data.get("user_types"));

                viewSignIn.setVisible(false);
                new Home();
            } else {
                JOptionPane.showMessageDialog(
                        viewSignIn,
                        "Incorrect Password.",
                        "Failed to Sign In",
                        JOptionPane.WARNING_MESSAGE
                );
            }
        }
    }

    protected void tenantSignUp() {
        String[] fields = {"username", "password", "passwordConfirm", "name", "phoneNumber", "identityNumber"};
        HashMap<String, String> fieldLabels = new HashMap<String, String>();
        HashMap<String, String> fieldValues = new HashMap<String, String>();
        String tmpValue;
        ArrayList<String> errorMessages = new ArrayList<String>();
        Users users = (Users) RentalSystem.getModel("users");

        fieldLabels.put("username", "Username");
        fieldLabels.put("password", "Password");
        fieldLabels.put("passwordConfirm", "Password (Confirm)");
        fieldLabels.put("name", "Name");
        fieldLabels.put("phoneNumber", "Phone Number");
        fieldLabels.put("identityNumber", "Identity Number");

        for (String fieldName : fields) {
            tmpValue = ((JTextField) data.get("textField" + upperCaseFirstCharacter(fieldName))).getText().trim();

            fieldValues.put(fieldName, tmpValue);

            if (tmpValue.equals("")) {
                errorMessages.add(fieldLabels.get(fieldName) + " is required.");
            }
        }

        if (users.usernameExist(fieldValues.get("username"))) {
            errorMessages.add("Username existed. Please try another.");
        }

        if (!fieldValues.get("password").equals(fieldValues.get("passwordConfirm"))) {
            errorMessages.add("Password and Password (Confirm) are not match.");
        }

        if (fieldValues.get("phoneNumber").matches("[^0-9]+")) {
            errorMessages.add("Phone Number only allow digits.");
        }

        if (fieldValues.get("identityNumber").matches("[^0-9]+")) {
            errorMessages.add("Identity Number only allow digits.");
        }

        if (!errorMessages.isEmpty()) {
            JOptionPane.showMessageDialog(
                    viewTenantSignUp,
                    String.join(System.getProperty("line.separator"), errorMessages),
                    "Form Errors",
                    JOptionPane.WARNING_MESSAGE
            );
        } else {
            PrimaryKeys primaryKeys = (PrimaryKeys) RentalSystem.getModel("primary_keys");
            TenantSignUpStatus tenantSignUpStatus = (TenantSignUpStatus) RentalSystem.getModel("tenant_sign_up_status");
            HashMap<String, String> userData = new HashMap<String, String>();
            HashMap<String, String> signUpData = new HashMap<String, String>();

            userData.put("username", fieldValues.get("username"));
            userData.put("password", fieldValues.get("password"));
            userData.put("user_types", Util.UserTypes.TENANT.toString());
            userData.put("name", fieldValues.get("name"));
            userData.put("phone_number", fieldValues.get("phoneNumber"));
            userData.put("identity_number", fieldValues.get("identityNumber"));
            users.insert(userData);

            signUpData.put("user_id", users.getInsertId());
            signUpData.put("status", Util.SignUpStatus.PENDING.toString());
            tenantSignUpStatus.insert(signUpData);

            JOptionPane.showMessageDialog(
                    viewTenantSignUp,
                    "Please allow 3 days working days for us to process your sign up. " +
                            "You may check your status at Check Tenant Sign Up Status page.",
                    "Successfully Signed Up",
                    JOptionPane.INFORMATION_MESSAGE
            );
        }
    }

    protected void checkSignUpStatus() {

    }
}
