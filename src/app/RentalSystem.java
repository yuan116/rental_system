package app;

import app.controllers.Auth;
import app.core.Session;
import app.models.PrimaryKeys;
import app.models.TenantSignUpStatus;
import app.models.Users;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class RentalSystem {
    protected static HashMap<String, Object> models = new HashMap<String, Object>();

    public static void main(String[] args) {
        models.put("primary_keys", new PrimaryKeys());
        models.put("tenant_sign_up_status", new TenantSignUpStatus());
        models.put("users", new Users());

        //https://stackoverflow.com/questions/12295056/gui-elements-not-showing-until-resize-of-window
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Auth();
            }
        });
    }

    public static Object getModel(String modelName) {
        if (modelName.equals("")) {
            throw new IllegalArgumentException("Model name cannot be empty string.");
        }

        Object model = models.get(modelName);
        if (model == null) {
            throw new NoSuchElementException(String.format("%s not found.", modelName));
        }

        return model;
    }

    public static void exitApplication() {
        Session.clearData();

        models.forEach((modelName, model) -> {
            try {
                //https://stackoverflow.com/questions/160970/how-do-i-invoke-a-java-method-when-given-the-method-name-as-a-string
                Method method = model.getClass().getMethod("exitApplication");
                method.invoke(model);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException event) {
                event.printStackTrace();
            }
        });

        System.exit(0);
    }
}
