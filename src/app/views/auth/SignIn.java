package app.views.auth;

import app.controllers.Auth;
import app.core.View;

import javax.swing.*;

public class SignIn extends View {
    private JPanel panelMain, panelButton;
    private JLabel labelTitle, labelUsername, labelPassword;
    private JTextField textFieldUsername, textFieldPassword;
    private JButton buttonSignIn, buttonGoToTenantSignUp, buttonGotoCheckSignUpStatus;

    public SignIn() {
        setVisible(true);
        setContentPane(panelMain);
        pack();

        actionListenerData.put("buttonSignIn", buttonSignIn);
        actionListenerData.put("buttonGoToTenantSignUp", buttonGoToTenantSignUp);
        actionListenerData.put("buttonGotoCheckSignUpStatus", buttonGotoCheckSignUpStatus);
        actionListenerData.put("textFieldUsername", textFieldUsername);
        actionListenerData.put("textFieldPassword", textFieldPassword);

        Auth authActionListener = new Auth(actionListenerData);
        buttonSignIn.addActionListener(authActionListener);
        buttonGoToTenantSignUp.addActionListener(authActionListener);
        buttonGotoCheckSignUpStatus.addActionListener(authActionListener);
    }
}
