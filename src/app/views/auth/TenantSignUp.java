package app.views.auth;

import app.controllers.Auth;
import app.core.View;

import javax.swing.*;

public class TenantSignUp extends View {
    private JPanel panelMain;
    private JLabel labelUsername, labelPassword, labelName, labelPasswordConfirm, labelPhoneNumber, labelIdentity;
    private JTextField textFieldUsername, textFieldPassword, textFieldPasswordConfirm, textFieldName, textFieldPhoneNumber, textFieldIdentityNumber;
    private JButton buttonTenantSignUp, buttonGoToSignIn;

    public TenantSignUp() {
        setContentPane(panelMain);
        pack();

        actionListenerData.put("buttonTenantSignUp", buttonTenantSignUp);
        actionListenerData.put("buttonGoToSignIn", buttonGoToSignIn);
        actionListenerData.put("textFieldUsername", textFieldUsername);
        actionListenerData.put("textFieldPassword", textFieldPassword);
        actionListenerData.put("textFieldName", textFieldName);
        actionListenerData.put("textFieldPasswordConfirm", textFieldPasswordConfirm);
        actionListenerData.put("textFieldPhoneNumber", textFieldPhoneNumber);
        actionListenerData.put("textFieldIdentityNumber", textFieldIdentityNumber);

        Auth authActionListener = new Auth(actionListenerData);
        buttonTenantSignUp.addActionListener(authActionListener);
        buttonGoToSignIn.addActionListener(authActionListener);
    }
}
